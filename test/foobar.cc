#include "stdio.h"

#define FLAG0 0
#define FLAG1 1

void foo(char* msg) {
    printf("%s", msg);
}

void switch_bar(int flag) {
    switch (flag)
    {
    case FLAG0:
        printf("%s", "bar for flag0\n");
        break;
    case FLAG1:
        printf("%s", "bar for flag1\n");
        break;
    default:
        printf("%s", "bar fail for flag \n");
        break;
    }
}

void indirect_foo(void (*func)(char*) , char* t) {
    func(t);
}

int main () {
    char* txt = "some foo test\n";
    indirect_foo(foo, txt);
    switch_bar(FLAG0);
    return 0;
}