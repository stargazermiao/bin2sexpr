#include "stdio.h"

#define FLAG0 0
#define FLAG1 1

void foo(char* msg) {
    printf("%s", msg);
}

void bar(char* msg) {
    foo(msg);
}

int main () {
    char* txt = "some foo test\n";
    foo(txt);
    return 0;
}