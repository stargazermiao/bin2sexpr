'''
TEST FILE
'''

from capstone import *
from elftools.elf.elffile import ELFFile


with open('./foobar', 'rb') as testfile:
    e = ELFFile(testfile)
    for section in e.iter_sections():
        print(hex(section['sh_addr']), section.name)

with open('./foobar', 'rb') as f:
    elf = ELFFile(f)
    code = elf.get_section_by_name('.text')
    ops = code.data()
    addr = code['sh_addr']
    md = Cs(CS_ARCH_X86, CS_MODE_64)
    for i in md.disasm(ops, addr):        
        print(f'0x{i.address:x}:\t{i.mnemonic}\t{i.op_str}')