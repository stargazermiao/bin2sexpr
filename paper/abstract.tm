<TeXmacs|1.99.12>

<style|<tuple|generic|british>>

<\body>
  <with|font-series|bold|Machine Model>

  c \<in\> Code := loaded program

  RealAddr :={ i \| [i] \<in\> Code }<nbsp>

  Aloc := (const addr \| (Stack [esp + const]) \| (Heap addr)) + Range

  <em|each [esp+const] means it is a var in some procedure, so everything
  here is finite.>

  <em|assume we have .symtab>

  <wide|Addr |^> := Label + RealAddr + Expr

  Syntax name is used here becasue one insn can change multi register/memeroy

  \;

  Control := Insn

  \<rho\> \<in\> <wide|Env|^> := (Aloc \| Reg) \<mapsto\> <wide|addr|^>\ 

  \<sigma\> \<in\> <wide|Store|^> := <wide|addr|^> \<mapsto\> (<wide|value|^>
  \| <wide|Kont|^>)

  \<kappa\> \<in\> <wide|Kont|^> := Env + CallsiteLabel + <wide|Addr|^>

  <with|font-shape|italic|font-size 9||address here is a pointer to next
  continuation>

  \<varsigma\> \<in\> <wide|State|^> := \<langle\>C, E, S, K*, Code\<rangle\>
  , \<sigma\>(K*) \<in\> <wide|Kont|^>

  \;

  <with|font-series|bold|Alloc/Update>

  0-CFA alloc:

  \;

  <wide|alloc|^>(l, addr, <math|expr>) :: Label \<rightarrow\> RealAddr
  \<rightarrow\> Expr \<rightarrow\> <wide|Addr|^>

  \;

  the actual trick to deal with break into a var/ptr align should be
  update/ref function

  \;

  calcAloc :: RealAddr \<rightarrow\> Aloc\ 

  <with|font-shape|right|a calculation function translate a real address into
  a set of abstract location. If it is invalid, throw segment fault
  <underline|>>\ 

  \;

  <with|font-series|bold|some abbreviation:>

  \<rho\>(aloc) \<Leftrightarrow\> ref(\<rho\>, aloc)

  \<sigma\>[(addr,range) \<mapsto\> v,<text-dots>] \<Leftrightarrow\>
  update(addr, range, v<text-dots>)

  ref(\<rho\>, (<math|l<rsub|0>>, <math|r<rsub|0>>)) := concat { <math|value
  in<around*|(|l<rsub|1>, \ r<rsub|0>\<cap\>r<rsub|1>|)>> \|
  (<math|l<rsub|1>>, <math|r<rsub|1>>) \<in\> key(\<rho\>),
  <math|l<rsub|1>=l<rsub|0>>, <math|r<rsub|0>\<cap\>r<rsub|1>\<neq\>\<emptyset\>>}

  update all <wide|addr|^> related to aloc, with respect of range then
  \<sqcup\> original \<sigma\>

  \;

  c(pc) \<Leftrightarrow\> fecthCodeByPC(code, pc)

  [reg] \<Leftrightarrow\> \<sigma\>(\<rho\>(reg))

  \;

  <with|font-series|bold|Stack Operation>

  only move esp

  <\eqnarray*>
    <tformat|<cwith|1|1|1|1|cell-halign|l>|<cwith|2|2|1|1|cell-halign|l>|<cwith|3|3|1|1|cell-halign|l>|<cwith|4|4|1|1|cell-halign|l>|<cwith|5|5|1|1|cell-halign|l>|<table|<row|<cell|\<langle\><around*|(|l,addr,<with|font-series|bold|pop
    > reg<rsub|0>|)>,\<rho\>,\<sigma\>,\<kappa\>\<star\>,c\<rangle\>>|<cell|<long-arrow|\<rubber-rightarrow\>|step>>|<cell|\<langle\>c<around*|(|addr+4|)>,\<rho\><around*|[|esp\<mapsto\>b,al<rsub|0>\<mapsto\>nv<rsub|0>|]>,\<sigma\><around*|[|b\<mapsto\><around*|[|esp|]>+4,nv<rsub|0>\<mapsto\><around*|[|esp|]>|]>\<sqcup\>\<sigma\>,\<kappa\>\<star\>,c\<rangle\>>>|<row|<cell|<with|font-series|bold|where><space|1em>\<sigma\>(\<rho\>(\<kappa\>*))
    = (\<rho\><rsub|\<kappa\>>,l<rsub|\<kappa\>>,next\<star\>)
    >|<cell|>|<cell|>>|<row|<cell|<space|4em>b = <wide|alloc|^>(l,addr,esp
    )>|<cell|>|<cell|>>|<row|<cell|<space|4em>nv<rsub|0>=<wide|alloc
    |^><around*|(|l,addr,reg<rsub|<rsub|0>>|)>>|<cell|>|<cell|>>|<row|<cell|<space|4em>al<rsub|0>=calcAloc
    <around*|(|<around*|[|reg<rsub|0>|]>|)>>|<cell|>|<cell|>>>>
  </eqnarray*>

  push similar

  \;

  <\with|font-series|bold>
    Memory Operation
  </with>

  \ 

  <\eqnarray*>
    <tformat|<cwith|1|1|1|1|cell-halign|l>|<cwith|2|2|1|1|cell-halign|l>|<cwith|4|4|1|1|cell-halign|l>|<cwith|5|5|1|1|cell-halign|l>|<cwith|3|3|1|1|cell-halign|l>|<cwith|6|6|1|1|cell-halign|l>|<table|<row|<cell|\<langle\><around*|(|l,addr,<with|font-series|bold|mov>
    op<rsub|0> op<rsub|1>|)>,\<rho\>,\<sigma\>,\<kappa\>\<star\>,c\<rangle\>>|<cell|<long-arrow|\<rubber-rightarrow\>|step>>|<cell|\<langle\>c<around*|(|addr+4|)>,\<rho\><around*|[|al<rsub|0>\<mapsto\>nv<rsub|0>|]>,\<sigma\><around*|[|nv<rsub|0>\<mapsto\>\<sigma\><around*|(|al<rsub|1>|)>|]>,k\<star\>,c\<rangle\>>>|<row|<cell|<with|font-series|bold|where><space|1em>op<rsub|0>
    is some realaddr>|<cell|>|<cell|>>|<row|<cell|<space|2em>\<sigma\>(\<rho\>(\<kappa\>*))
    = (\<rho\><rsub|\<kappa\>>,l<rsub|\<kappa\>>,next\<star\>)
    >|<cell|>|<cell|>>|<row|<cell|<space|2em>nv<rsub|0>=<wide|alloc|^><around*|(|l,addr,op<rsub|0>|)>>|<cell|>|<cell|>>|<row|<cell|<space|2em>al<rsub|0>=calcAloc<around*|(|op<rsub|0>|)><space|2em>>|<cell|>|<cell|>>|<row|<cell|<space|2em>al<rsub|1>=calcAloc<around*|(|op<rsub|1>|)>>|<cell|>|<cell|>>>>
  </eqnarray*>

  \;

  \;

  push rbp

  \;

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|font-base-size|10>
  </collection>
</initial>