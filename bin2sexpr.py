'''
a tool to dump binary into sexpr

Yihao Sun<ysun67@syr.edu>
2020 Syracuse
'''
import sys

from elftools.elf.elffile import ELFFile
from capstone import CsInsn, Cs, CS_ARCH_X86, CS_MODE_64

SECTION_USED = ['.text', '.plt', '.init', '.fini']

PTR_INFO = ['qword', 'dword', 'ptr', 'word', 'byte']

def lstr_to_sexpr(pylist):
    '''
    str -> str

    util function to convert a python SPACE seped string into sexpr list string
    '''
    def _trim_s(s):
        return ' '.join(filter((lambda x: x not in PTR_INFO), s.split(' ')))
    sexpr = '('
    for s in map(_trim_s, pylist.split(', ')):
        sexpr += '{} '.format(s)
    sexpr += ')'
    sexpr = sexpr.replace('0x', '#x')
    return sexpr


class BinFile:
    '''
    ADT for binary file, more info data should be added here
    '''

    def __init__(self):
        self.arch = 'x86_64'
        self.code = {}
        self.symtab = {}
        self.sec_base = {}

    def to_sexpr(self):
        '''
        dump the binary file into s-expression

        bin? := (,arch (list section))
        section? := (,name ,sec_data)
        sec_data? := code | plt
        code? := (,addr ,mnemonic ,op)
        op? := this should base on x86 ISA
        '''
        sexpr = '('
        sexpr += self.arch + ' \n'
        # symtable
        sexpr += ' ('
        sexpr += '.symtab ('
        for s, a in self.symtab.items():
            sexpr += '({} #x{:x}) '.format(s, a)
        sexpr += '))\n'
        sexpr += ' (\n'
        for s in SECTION_USED:
            # text section
            sexpr += ' ('
            sexpr += '{} #x{:x}\n  #(\n'.format(s, self.sec_base[s])
            for inst in self.code[s]:
                sexpr += '    (#x{:x} {} {})\n'.format(inst.address,
                                                    inst.mnemonic,
                                                    lstr_to_sexpr(inst.op_str))
            sexpr += '   ))\n'

        sexpr += '))'
        return sexpr


def disam_all(name):
    '''
    str -> BinFile

    given the name of a file, dsiasm all segment
    return the disasmed code in capstone's CsInsn ADT
    '''
    bfile = BinFile()
    with open(name, 'rb') as input_file:
        elf = ELFFile(input_file)
        plt_data = elf.get_section_by_name('.plt')
        text_data = elf.get_section_by_name('.text')
        init_data = elf.get_section_by_name('.init')
        fini_data = elf.get_section_by_name('.fini')
        symtab_data = elf.get_section_by_name('.symtab')
        if elf.elfclass == 32:
          raise Exception("32bit not support!\n")
        md = Cs(CS_ARCH_X86, CS_MODE_64)
        bfile.sec_base['.text'] = text_data['sh_addr']
        bfile.code['.text'] = md.disasm(text_data.data(), text_data['sh_addr'])
        bfile.sec_base['.plt'] = plt_data['sh_addr']
        bfile.code['.plt'] = md.disasm(plt_data.data(), plt_data['sh_addr'])
        bfile.sec_base['.init'] = init_data['sh_addr']
        bfile.code['.init'] = md.disasm(init_data.data(), init_data['sh_addr'])
        bfile.sec_base['.fini'] = fini_data['sh_addr']
        bfile.code['.fini'] = md.disasm(fini_data.data(), fini_data['sh_addr'])
        if symtab_data is not None:
            for t in symtab_data.iter_symbols():
                if t.name != '':
                    bfile.symtab[t.name] = t.entry['st_value']
    return bfile


if __name__ == "__main__":
    # omit some arg parse here....
    filename = sys.argv[1]
    bfile = disam_all(filename)
    print(bfile.to_sexpr())
