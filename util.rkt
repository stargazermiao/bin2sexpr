;; some util function

#lang racket

(provide (all-defined-out))

(define (get-addr-insn i)
  (match i
    [`(,l ,ri) (first ri)]
    [else #f]))

;; find a line of code in a segment
(define (find-addr-seg sec addr)
  (match sec
    [`(,name ,base ,code)
     (let ([found (filter (λ (x)
                            (equal? (get-addr-insn x) addr))
                          (vector->list code))])
       (if (empty? found)
           ;; assume code addr never dupllicate
           #f
           (first found)))]))

;; find a line of code by it's address in code segment
(define (find-addr-prog addr p)
  (match p
    [`(,arch ,symtab ,secs)
     ;; assume addr never duplicate
     (foldl (λ (s res) (find-addr-seg s addr)) #f secs)]))

(define (find-index-seg i sec)
  (match sec
    [`(,name ,base ,code) (vector-ref code i)]))

(define (find-index-prog p l i)
  (match p
    [`(,arch ,symtab ,secs)
     ;; assume addr never duplicate
     (let/ec return
       (and (map (λ (s)
                   (if (equal? l (first s))
                       (return (vector-ref (third s) i))
                       #f))
                 secs)))]))

(define (find-index-proc p i)
  (match p
    [`(,name ,addr ,code)
     (if (< i (vector-length code))
         (vector-ref code i)
         #f)]))

;; get the first insn of a program
(define (find-first-insn prog)
  (match prog
    [`(,arch ,symtab ,secs)
     (find-addr-prog (hash-ref symtab 'main) prog)]))

(define (find-next-insn a prog)
  (let/ec return
    (map
     (λ (sec)
       (foldl (λ (i res)
                (match i
                  [`(,l (,ia _ _))
                   (if res
                       (not (equal? ia))
                       (return ia))]))
              #t (vector->list (third sec))))
     (third prog))))

;; if access fail return #f
(define (vector-ref-e v i)
  (if (< i (vector-length v))
      (vector-ref v i)
      #f))

;; PC++
(define (next i prog)
  (match i
    [`(,l (,a ,m ,ops)) `(,l ,(find-addr-prog (add1 a) prog))]))

;; find the closest label of a given line of code
;; find-nearsest-label :: Prog → Addr → label
(define (find-nearest-label prog pos)
  (match prog
    [`(,arch ,symtab ,secs)
     (call/ec
      (λ (return)
        (map (λ (sec)
               (match sec
                 [`(,name ,base ,code)
                  (foldl (λ (i res)
                           (let ([cp (first (second i))]
                                 [labels (first i)])
                             (if (equal? cp pos)
                                 (return res)
                                 (if (empty? labels) res labels))))
                         '() (vector->list code))]))
             secs)))]))

;; get list of lable of a given code addrs, default using a '()
(define (get-label-by-val h v)
  (foldl (λ (k res) (if (equal? (hash-ref h k) v)
                         (cons k res)
                         res))
         '() (hash-keys h)))

