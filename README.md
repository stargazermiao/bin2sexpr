# bin2sexpr

this project is a tool to disasmble a ELF binary (using [capstone](https://www.capstone-engine.org/lang_python.html)) and dump the asm code into s-expression.



### requirement

 python>=3.6
 capstone==4.0.1
 pyelftools==0.26


### test case
- foobar : a simple printf
 

### how to use

> python  bin2sexpr.py  ./test/foobar

### how to run test server
> racket server.rkt

- `/start` POST {"file": [file]}